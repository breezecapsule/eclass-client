import React, { Component } from 'react';
import {
    AppRegistry
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Provider } from 'react-redux';

import Root from './containers/Root';
import configureStore  from './store/configureStore';

const store = configureStore();
EStyleSheet.build();

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Root />
            </Provider>
        );
    }
}

export default App;
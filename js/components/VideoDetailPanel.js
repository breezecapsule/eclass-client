import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    TouchableOpacity
} from 'react-native';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import Video from 'react-native-video';

import AppActions from '../actions/AppActions';

const styles = EStyleSheet.create({
    container: {
        position: 'relative',
        flex: 1,
        flexDirection:'column'
    },
    header: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        backgroundColor: 'lightgray'
    },
    headerText: {
        color: '#006400',
        fontSize: 32,
        fontWeight: 'bold'
    },
    content: {
        flex: 1,
        flexDirection:'column'
    },
    videoTitle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'green',
        flex: 0.1
    },
    back: {
        position: 'absolute',
        top: 15,
        left: 10
    },
    backText: {
        color: 'grey',
        fontSize: 24,
        fontWeight: 'bold'
    },
    videoTitleText: {
        color: 'white',
        fontSize: 28,
        fontWeight: 'bold'
    },
    video: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0.5
    },
    videoTeacher: {
        height: 40,
        paddingLeft: 10,
        paddingRight: 10
    },
    videoTeacherText: {
        height: 40,
        color: 'black',
        fontSize: 16,
        fontWeight: 'bold'
    },
    videoDescription: {
        width: '100%',
        flex: 0.4,
        paddingLeft: 10,
        paddingRight: 10
    },
    videoDescriptionText: {
        fontSize: 16,
        color: 'black'
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    progress: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 3,
        overflow: 'hidden'
    },
    trackingControls: {
        flexDirection: 'row',
        backgroundColor: "gray",
        marginBottom: 10
    },
    icon: {
        width: 15,
        height: 15
    },
    innerProgressCompleted: {
        height: 15,
        borderTopRightRadius: 8,
        borderBottomRightRadius: 8,
        backgroundColor: '#cccccc'
    },
    innerProgressRemaining: {
        height: 15,
        backgroundColor: '#2C2C2C'
    }
});

class VideoDetailPanel extends Component {
    constructor(props) {
        super(props);
        this.onLoad = this.onLoad.bind(this);
        this.onProgress = this.onProgress.bind(this);
        this.onEnd = this.onEnd.bind(this);
        this.state = {
            rate: 1,
            volume: 1,
            muted: false,
            resizeMode: 'contain',
            duration: 0.0,
            currentTime: 0.0
        };
    }

    onLoad(data) {
        this.setState({
            duration: data.duration
        });
    }

    onProgress(data) {
        this.setState({currentTime: data.currentTime});
    }

    onEnd(data) {
        this.setState({
            rate: 1,
            volume: 1,
            muted: false,
            resizeMode: 'contain',
            duration: 0.0,
            currentTime: 0.0
        });
    }

    getCurrentTimePercentage() {
        if (this.state.currentTime > 0) {
            return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
        } else {
            return 0;
        }
    }

    _onPressBack() {
        this.props.dispatch(AppActions.Actions.SwitchPanel("VideoListPanel"));
    }

    render() {
        const video = this.props.app.selectedVideo;
        const flexCompleted = this.getCurrentTimePercentage() * 100;
        const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>
                        鲁虹在线
                    </Text>
                    <TouchableOpacity style={styles.back} onPress={this._onPressBack.bind(this)}>
                        <Text style={styles.backText}>
                            返回
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.content}>
                    <View style={styles.videoTitle}>
                        <Text style={styles.videoTitleText}>
                            {video.subject}
                        </Text>
                    </View>
                    <View style={styles.video}>
                        <Text>加载中...</Text>

                        <TouchableOpacity style={styles.fullScreen} onPress={() => {this.setState({paused: !this.state.paused})}}>
                            <Video source={{uri: video.fullpath}}
                                   style={styles.fullScreen}
                                   rate={this.state.rate}
                                   paused={this.state.paused}
                                   volume={this.state.volume}
                                   muted={this.state.muted}
                                   resizeMode={this.state.resizeMode}
                                   onLoad={this.onLoad}
                                   onProgress={this.onProgress}
                                   onEnd={this.onEnd}
                                   repeat={true} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.trackingControls}>
                        <TouchableOpacity onPress={() => {this.setState({paused: !this.state.paused})}}>
                            {
                                this.state.paused ?
                                    <Image
                                        style={styles.icon}
                                        source={require('../images/play.png')}
                                        />
                                    :
                                    <Image
                                        style={styles.icon}
                                        source={require('../images/pause.png')}
                                        />
                            }
                        </TouchableOpacity>
                        <View style={styles.progress}>
                            <View style={[styles.innerProgressCompleted, {flex: flexCompleted}]} />
                            <View style={[styles.innerProgressRemaining, {flex: flexRemaining}]} />
                        </View>
                    </View>

                    <View style={styles.videoTeacher}>
                        <Text style={styles.videoTeacherText}>
                            {video.teacher.fullname}
                        </Text>
                    </View>
                    <View style={styles.videoDescription}>
                        <Text style={styles.videoDescriptionText}>
                            {video.description}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }

}

let componentState = ({root}) => ({
    app: root.get('app').toJS()
});

export default connect(componentState)(VideoDetailPanel);
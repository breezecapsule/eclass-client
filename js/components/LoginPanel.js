import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity
} from 'react-native';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';

import AppActions from '../actions/AppActions';

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        flexDirection:'column'
    },
    header: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        backgroundColor: 'lightgray',
    },
    headerText: {
        color: '#006400',
        fontSize: 32,
        fontWeight: 'bold',
    },
    loginBox: {
        width: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    signinButton: {
        width: 120,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'grey'
    }

});

class LoginPanel extends Component {
    constructor(props) {
        super(props);
        this.state ={
            username: '',
            password: ''
        }
    }

    _onTouchLogin() {
        if (this.state.username.length === 0) {
            Alert.alert(
                '提示',
                '请输入您的用户名！'
            );
            return
        }
        this.props.dispatch(AppActions.Actions.SubmitAuthorizeData({username: this.state.username, password: this.state.password}));
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>
                        鲁虹在线
                    </Text>
                </View>
                <View style={styles.loginBox}>
                    <TextInput
                        style={{width: 180, height: 40, borderColor: 'gray', borderWidth: 1}}
                        onChangeText={(text) => this.setState({username: text})}
                        placeholder={'用户名'}
                        value={this.state.username}
                        />
                    <TextInput
                        style={{width: 180, height: 40, borderColor: 'gray', borderWidth: 1}}
                        onChangeText={(text) => this.setState({password: text})}
                        placeholder={'密码'}
                        password={true}
                        value={this.state.password}
                        />
                    <TouchableOpacity style={styles.signinButton} onPress={this._onTouchLogin.bind(this)}>
                        <Text>
                            登录
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

}

let componentState = ({root}) => ({
    app: root.get('app').toJS()
});

export default connect(componentState)(LoginPanel);
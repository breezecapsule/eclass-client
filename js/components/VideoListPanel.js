import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
    TextInput,
    ListView,
    ScrollView,
    Image,
    TouchableOpacity
} from 'react-native';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';

import AppActions from '../actions/AppActions';

const styles = EStyleSheet.create({
    container: {
        position: 'relative',
        flex: 1,
        flexDirection:'column'
    },
    header: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        backgroundColor: 'lightgray',
    },
    headerText: {
        color: '#006400',
        fontSize: 32,
        fontWeight: 'bold',
    },
    content: {
        flex: 1,
        flexDirection:'row'
    },

    videoCategory: {
        width: '40%',
        flex: 1,
        backgroundColor: 'lightgray'
    },

    videoList: {
        width: '60%',
        flex: 1,
        backgroundColor: 'white'
    },

    categoryTitle: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'green'
    },

    categoryTitleText: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'white',
    },

    categoryItem: {
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 5
    },
    categoryItem_active: {
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 5,
        backgroundColor: 'white'
    },
    categoryText: {
        width: 150,
        fontSize: 18,
        fontWeight: 'bold'
    },

    categoryItemVideoCount: {
        position: 'absolute',
        top: 30,
        right: 5,
        width: 20,
        textAlign: 'center',
        fontSize: 16,
        paddingLeft: 5,
        color: 'white',
        backgroundColor: 'green'
    },

    videoItem: {
        height: 220,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5
    },

    videoCover: {
        width: 200,
        height: 150,
        borderWidth: 1,
        borderColor: 'lightgray',
    },

    videoSubject: {
        width: 200,
        textAlign: 'left',
        color: 'black',
        fontWeight: 'bold',
        marginTop: 5
    },

    videoTeacher: {
        width: 200,
        textAlign: 'left',
        color: 'lightgray',
        fontWeight: 'bold',
        marginTop: 5
    },

});

class VideoListPanel extends Component {
    constructor(props) {
        super(props);
    }

    _onPressCategory(categoryId) {
        this.props.dispatch(AppActions.Actions.SelectVideoCategory(categoryId));
    }

    _onPressVideo(video) {
        this.props.dispatch(AppActions.Actions.SelectVideo(video));
    }

    _renderCategoryRow(rowData) {
        const categoryId = rowData[0];
        const categoryText = rowData[1];
        let categoryItemClass = this.props.app.selectedCategory === categoryId ? styles.categoryItem_active : styles.categoryItem;
        return  (
            <TouchableOpacity style={categoryItemClass} onPress={this._onPressCategory.bind(this, categoryId)}>
                <Text style={styles.categoryText} >
                    {categoryText}
               </Text>
                {this.props.app.selectedCategory === categoryId ? <Text style={styles.categoryItemVideoCount} >{this.props.app.selectedVideos.totalCount}</Text> : null}
            </TouchableOpacity>
        )
    }

    _renderVideoRow(rowData) {
        return (
            <TouchableOpacity style={styles.videoItem} onPress={this._onPressVideo.bind(this, rowData)}>
                <Image style={styles.videoCover} source={{uri: 'http://121.40.174.231/'+ rowData.cover}}/>
                <Text style={styles.videoSubject}>{rowData.subject}</Text>
                <Text style={styles.videoTeacher}>{rowData.teacher.fullname}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        var categoriesDatasource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        let categories = [];
        if ( this.props.app.categories == null) {
            categories = categoriesDatasource.cloneWithRows([]);
        } else {
            categories = categoriesDatasource.cloneWithRows(this.props.app.categories);
        }

        var videosDatasource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        let videos = null;
        if ( this.props.app.selectedVideos == null) {
            videos = videosDatasource.cloneWithRows([]);
        } else {
            videos = videosDatasource.cloneWithRows(this.props.app.selectedVideos.listData);
        }
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>
                        鲁虹在线
                    </Text>
                </View>
                <View style={styles.content}>
                    <View style={styles.videoCategory}>
                        <View style={styles.categoryTitle}>
                            <Text style={styles.categoryTitleText}>视频</Text>
                        </View>
                        <ListView
                            dataSource={categories}
                            renderRow={this._renderCategoryRow.bind(this)}
                            />
                    </View>
                    <View style={styles.videoList}>
                        <ListView
                            dataSource={videos}
                            renderRow={this._renderVideoRow.bind(this)}
                            />
                    </View>
                </View>
            </View>
        );
    }

}

let componentState = ({root}) => ({
    app: root.get('app').toJS()
});

export default connect(componentState)(VideoListPanel);
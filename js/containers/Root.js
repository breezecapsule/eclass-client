import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import AppActions from '../actions/AppActions';

import LoginPanel from '../components/LoginPanel';
import VideoListPanel from '../components/VideoListPanel';
import VideoDetailPanel from '../components/VideoDetailPanel';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class Root extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(AppActions.Actions.InitializeApp({status: 'Initializing'}));
    }

    render() {
        let currentPanel = (<View></View>);
        switch (this.props.app.currentPanel) {
            case 'LoginPanel': currentPanel = (<LoginPanel />);break;
            case 'VideoListPanel': currentPanel = (<VideoListPanel />);break;
            case 'VideoDetailPanel': currentPanel = (<VideoDetailPanel />);break;
            default : currentPanel = (<View></View>);break;
        }
        return (
          <View style={styles.container}>
              {currentPanel}
          </View>
        );
    }

}

let componentState = ({root}) => ({
    app: root.get('app').toJS()
});

export default connect(componentState)(Root);
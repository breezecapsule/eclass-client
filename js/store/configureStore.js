import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';

import rootReducer from '../reducers/Reducers';

const reducer = combineReducers({
    root: rootReducer
});

const logger = createLogger({
    stateTransformer(state) {
        return state.root.toJS();
    }
});

const enhancer = compose(
    applyMiddleware(thunk, logger)
);

export default function configureStore(initialState) {
    return createStore(reducer, initialState, enhancer);
}
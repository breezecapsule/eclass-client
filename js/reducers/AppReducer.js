import Immutable from 'immutable';

import AppActions from '../actions/AppActions';

const DEFAULT_APP_STATE = {
    status: 'NOT_LOGIN',
    currentPanel: 'LoginPanel',
    categories: null,
    currentUser: null,
    selectedCategory: null,
    selectedVideos: null,
    selectedVideo: null
};

function app(state, action) {
    const currentState = state || Immutable.Map(DEFAULT_APP_STATE);
    switch (action.type) {
        case AppActions.Keys.InitializeApp:
            return currentState.merge(action.args);
        case AppActions.Keys.InitializeAppCompleted:
            return currentState.merge(action.args);
        case AppActions.Keys.SwitchPanel:
            return currentState.merge({
                currentPanel: action.args
            });
        case AppActions.Keys.SubmitAuthorizeDataCompleted:
            return currentState.merge({
                currentUser: Immutable.Map(action.args)
            });
        case AppActions.Keys.LoadVideoCategoriesCompleted:
            return currentState.merge({
                categories: Immutable.List(action.args)
            });
        case AppActions.Keys.SelectVideoCategory:
            return currentState.merge({
                selectedCategory: action.args
            });
        case AppActions.Keys.SelectVideoCategoryCompleted:
            return currentState.merge({
                currentPanel: 'VideoListPanel',
                selectedVideos: action.args
            });
        case AppActions.Keys.SelectVideo:
            return currentState.merge({
                currentPanel: 'VideoDetailPanel',
                selectedVideo: Immutable.Map(action.args)
            });
        default:
            return currentState;
    }
}

module.exports = app;

import {createActions} from '../utils/ActionsBuilder';
import _ from 'lodash';

function initializeApp() {
    return new Promise((resolve) => {
        //_.delay(() => {
        //    resolve();
        //}, 100);
        resolve()
    });
}

module.exports = createActions({

    InitializeApp(args) {
        return this.dispatchMe(args)
            .then(() => initializeApp(args)
                .then(() =>
                    this.dispatch(
                        this.Actions.InitializeAppCompleted({status: 'Initialized'})
                    ))
        );
    },

    SubmitAuthorizeData(args) {
        return this.dispatchMe(args)
            .then(() =>
                fetch("http://121.40.174.231/api/v1/tokens", {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            username: args.username,
                            password: args.password
                        })
                    }
                )
                .then((response) => response.json())
                .then((responseData) => {
                    return this.dispatch(this.Actions.SubmitAuthorizeDataCompleted(responseData))
                        .then(() => this.dispatch(this.Actions.LoadVideoCategories()))
                })
                .done()
            )
    },

    LoadVideoCategories(args) {
        const currentUser = this.getCurrentState().root.toJS().app.currentUser;
        const authorizationToken = ''+ currentUser.token_type + ' ' + currentUser.access_token;
        return this.dispatchMe(args)
            .then(() =>
                fetch("http://121.40.174.231/api/categories", {
                        method: "GET",
                        headers: {
                            'Accept': 'application/json',
                            'Authorization': authorizationToken,
                            'Content-Type': 'application/json'
                        }
                    }
                )
                .then((response) => response.json())
                .then((responseData) => {
                    return this.dispatch(this.Actions.LoadVideoCategoriesCompleted(responseData.data))
                        .then(() => this.dispatch(this.Actions.SelectVideoCategory(responseData.data[0][0])))
                })
                .done()
        )
    },

    SelectVideoCategory(args) {
        const currentUser = this.getCurrentState().root.toJS().app.currentUser;
        const authorizationToken = ''+ currentUser.token_type + ' ' + currentUser.access_token;
        return this.dispatchMe(args)
            .then(() =>
                fetch("http://121.40.174.231/api/videos?currentPage=0&pageSize=10&categoryId="+ this.getCurrentState().root.toJS().app.selectedCategory, {
                        method: "GET",
                        headers: {
                            'Accept': 'application/json',
                            'Authorization': authorizationToken,
                            'Content-Type': 'application/json'
                        }
                    }
                )
                .then((response) => response.json())
                .then((responseData) => {
                    return this.dispatch(this.Actions.SelectVideoCategoryCompleted(responseData.data))
                })
                .done()
        )
    },

    SelectVideo(args) {
        return this.dispatchMe(args)
    },

    SwitchPanel(args) {
        return this.dispatchMe(args)
    }
})